﻿CREATE TABLE [dbo].[Liquors]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [Color] VARCHAR(50) NULL, 
    [Description] TEXT NULL
)
