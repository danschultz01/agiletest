﻿using System.Linq;
using AgileTest.Models;

namespace AgileTest.Repositories
{
    public class LiquorTypeRepository: ILiquorTypeRepository
    {
        
        private readonly AgileTestContext _context;

        public LiquorTypeRepository()
        {
            _context = new AgileTestContext();
        }

        /// <summary>
        /// Returns an IQueryable list of LiquorTypes
        /// </summary>
        /// <returns></returns>
        public IQueryable<LiquorType> GetAll()
        {
            return _context.LiquorTypes;
        }

        /// <summary>
        /// Returns a specific LiquorType based on id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LiquorType GetById(int id)
        {
            return _context.LiquorTypes.Single(lt => lt.Id == id);
        }

        /// <summary>
        /// Add's a new LiquorType to the context and saves
        /// </summary>
        /// <param name="liquorType"></param>
        /// <returns></returns>
        public LiquorType SaveNewLiquorType(LiquorType liquorType)
        {
            _context.LiquorTypes.Add(liquorType);
            SaveLiquorType();
            return liquorType;
        }

        /// <summary>
        /// Saves all changes to the db context
        /// </summary>
        public void SaveLiquorType()
        {
            _context.SaveChanges();

        }

    }
}