﻿using System.Linq;
using AgileTest.Models;

namespace AgileTest.Repositories
{
    interface ILiquorTypeRepository
    {
        IQueryable<LiquorType> GetAll();
        LiquorType GetById(int id);
        LiquorType SaveNewLiquorType(LiquorType liquorType);
        void SaveLiquorType();
    }
}