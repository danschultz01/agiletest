﻿using System.Linq;
using AgileTest.Models;

namespace AgileTest.Repositories
{
    public class LiquorRepository: ILiquorRepository
    {
        

        private readonly AgileTestContext _context;

        public LiquorRepository()
        {
            _context = new AgileTestContext();
        }

        /// <summary>
        /// Returns an IQueryable list of Liquors
        /// </summary>
        /// <returns></returns>
        public IQueryable<Liquor> GetAll()
        {
            return _context.Liquors;
        }

        /// <summary>
        /// Returns a specific Liquor by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Liquor GetById(int id)
        {
            return _context.Liquors.Single(lt => lt.Id == id);
        }

        /// <summary>
        /// Add's a new Liquor to the context and saves
        /// </summary>
        /// <param name="liquor"></param>
        /// <returns></returns>
        public Liquor SaveNewLiquor(Liquor liquor)
        {
            _context.Liquors.Add(liquor);
            SaveLiquor();
            return liquor;
        }

        //TODO: convert to async
        /// <summary>
        /// Add's a new rating to a specific liquor and returns the updated liqour
        /// </summary>
        /// <param name="liquorId"></param>
        /// <param name="rating"></param>
        /// <returns></returns>
        public Liquor AddNewRating(int liquorId, int rating)
        {
            var liquor = _context.Liquors.Single(l => l.Id == liquorId);
            if (liquor != null)
            {
                liquor.Reviews.Add(new Review
                {
                    Rating = rating
                });
            }
            SaveLiquor();
            _context.Entry<Liquor>(liquor).Reload();
            return liquor;
        }

        /// <summary>
        /// Saves all changes to the db context
        /// </summary>
        public void SaveLiquor()
        {
            _context.SaveChanges();

        }

    }
}