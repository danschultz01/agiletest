﻿using System.Linq;
using AgileTest.Models;

namespace AgileTest.Repositories
{
    interface ILiquorRepository
    {
        IQueryable<Liquor> GetAll();
        Liquor GetById(int id);
        Liquor SaveNewLiquor(Liquor liquor);
        Liquor AddNewRating(int liquorId, int rating);
        void SaveLiquor();
    }
}