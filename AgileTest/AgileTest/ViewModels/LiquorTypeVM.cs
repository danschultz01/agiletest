﻿using System.ComponentModel.DataAnnotations;

namespace AgileTest.ViewModels
{
    public class LiquorTypeVm
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

    }
}