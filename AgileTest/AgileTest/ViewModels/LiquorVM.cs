﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;


namespace AgileTest.ViewModels
{
    public class LiquorVm
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Color { get; set; }
        public string Description { get; set; }

        [Required]
        [Display(Name = "Category")]
        public int? LiquorTypeId { get; set; }
        public int TotalReviews { get; set; }
        public int? TotalStars { get; set; }


        /// <summary>
        /// Calculate the average stars based on TotalStars/TotalReviews
        /// </summary>
        public string AverageReview
        {
            get
            {
                if (TotalReviews == 0 || TotalStars == null)
                {
                    return "n/a";
                }
                var averageStars = Math.Round((double)TotalStars.Value/(double)TotalReviews, 1);
                return averageStars.ToString(CultureInfo.InvariantCulture);
            }
            
        }

        public LiquorTypeVm LiquorTypeVm { get; set; }
    }
}