﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AgileTest.Assemblers;
using AgileTest.Repositories;
using AgileTest.ViewModels;

namespace AgileTest.Controllers
{
    public class HomeController : Controller
    {
        #region properties
        private readonly ILiquorAssembler _liquorAssembler;
        private readonly ILiquorRepository _liquorRepository;
        private readonly ILiquorTypeAssembler _liquorTypeAssembler;
        private readonly ILiquorTypeRepository _liquorTypeRepository;
        #endregion

        #region Constructor
        public HomeController()
        {
            // Initalize the properties (Typically use dependency injection, but not sure which one Agile uses)
            _liquorRepository = new LiquorRepository();
            _liquorAssembler = new LiquorAssembler();
            _liquorTypeRepository = new LiquorTypeRepository();
            _liquorTypeAssembler = new LiquorTypeAssembler();
        }
        #endregion

        public ActionResult Index()
        {
            ViewBag.Title = "Liquor Library";
            return View();
        }


        #region LiquorType / Categories
        [HttpGet]
        public PartialViewResult LoadCategories()
        {
            ViewData["LiquorTypeVms"] = GetLiquorTypeVms();
            return PartialView("Partials/LoadCategories");
        }

        [HttpGet]
        public PartialViewResult AddCategory()
        {
            var vm = new LiquorTypeVm();
            return PartialView("Partials/AddCategory",vm);
        }

        [HttpPost]
        public PartialViewResult AddCategory(LiquorTypeVm liquorTypeVm)
        {
            var liquorType = _liquorTypeAssembler.Assemble(liquorTypeVm);
            // Even though liquorType is not used I like to return it with the ID in case I need it later
            liquorType = _liquorTypeRepository.SaveNewLiquorType(liquorType);
            ViewData["LiquorTypeVms"] = GetLiquorTypeVms();
            return PartialView("Partials/LoadCategories");
        }

        [HttpPost]
        public JsonResult SaveRating(int rating, int liquorId)
        {
            var liquor = _liquorRepository.AddNewRating(liquorId, rating);
            var liquorVm = _liquorAssembler.Disassemble(liquor);
            return Json(liquorVm);
        }
        #endregion

        #region Liquors

        [HttpGet]
        public PartialViewResult LoadLiquors(int? liquorTypeId)
        {
            var liquorVms = GetLiquorVms();
            if (liquorTypeId != null)
            {
                liquorVms = liquorVms.Where(l => l.LiquorTypeId == liquorTypeId).ToList();
            }
            ViewData["LiquorVms"] = liquorVms;
            return PartialView("Partials/LoadLiquors");
        }

        [HttpGet]
        public PartialViewResult AddLiquor()
        {
            var vm = new LiquorVm();
            ViewData["LiquorTypesSelectList"] = GetLiquorTypeSelectList();
            return PartialView("Partials/AddLiquor",vm);
        }

        [HttpPost]
        public PartialViewResult AddLiquor(LiquorVm liquorVm)
        {
            var liquor = _liquorAssembler.Assemble(liquorVm);
            // Even though liquor is not used I like to return it with the ID in case I need it later
            liquor = _liquorRepository.SaveNewLiquor(liquor);
            ViewData["LiquorVms"] = GetLiquorVms();
            return PartialView("Partials/LoadLiquors");
        }
        #endregion

        #region Helper Methods

        private List<LiquorTypeVm> GetLiquorTypeVms()
        {
            var liquorTypes = _liquorTypeRepository.GetAll().ToList();
            return liquorTypes.Select(c => _liquorTypeAssembler.Disassemble(c)).ToList();
        }

        private List<LiquorVm> GetLiquorVms()
        {
            var liquors = _liquorRepository.GetAll().Include(l => l.LiquorType).ToList();
            return liquors.Select(c => _liquorAssembler.Disassemble(c)).ToList();
        }

        private List<SelectListItem> GetLiquorTypeSelectList()
        {
            var liquorTypes = _liquorTypeRepository.GetAll().ToList();
            return liquorTypes.Select(l => new SelectListItem {Text = l.Name, Value = l.Id.ToString()}).ToList();
        } 

        #endregion

    }
}
