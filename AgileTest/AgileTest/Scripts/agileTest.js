﻿$(document).ready(function () {
    // Initalize the AgileTest Ajax Content Controller
    //   Ideally this would be Angular2.js, but I already know how to do it in jQuery.
    AtController.Init();
});

/// AgileTest Content Controller
///   Handles all communication with server 
var AtController = {
    // Properties
    CurrentAction: null,
    Loading: false,
    SameClickCount: 0,
    TopNav: null,
    ContentTitle: null,
    ContentBody: null,

    // Intialize
    Init: function() {
        // Configure properties so we don't search dom multiple times
        AtController.TopNav = $("#topNav");
        AtController.ContentTitle = $("#contentTitle");
        AtController.ContentBody = $("#contentBody");

        // Configure the navigation buttons
        $(AtController.TopNav).on("click", "a", function() {
            AtController.TopNavClickHandler($(this));
        });

        // Configure form submission buttons
        $(AtController.ContentBody).on("click", ".submitButton", function() {
            AtController.SubmitForm();
        });

        // Configure selection of a category
        $(AtController.ContentBody).on("click", ".liquorType", function() {
            if (!AtController.Loading) {
                //TODO: move this into data
                // Get the category name clicked to display on page title
                var category = $(this).find(".name").text();
                // We are going to display the liquors page 
                AtController.CurrentAction = "LoadLiquors";
                // Load the content
                AtController.LoadPage($(this).data("id"));
                // Set the top nav button to be liquors
                AtController.SetActiveNav($("#liquors"));
                // Set the page title
                AtController.SetContentTitle("Liquors - Category:  " + category);

            }

        });

        // Configure rating buttons 
        //TODO: Convert to stars
        $(AtController.ContentBody).on("click", ".rating a", function() {
            if (!AtController.Loading) {
                AtController.SaveRating($(this));
            }
        });
    },

    // Save the rating and display the current votes
    SaveRating: function(ratingButton) {
        var rating = $(ratingButton).text();
        var liquorId = $(ratingButton).closest(".liquor").data("id");
        var postVars = {
            rating: rating,
            liquorId: liquorId
        };
        $.ajax({
            url: "/Home/SaveRating",
            data: postVars,
            type: "POST",
            success: function(jsonResults) {
                var displayValue = "Rating: " +
                    jsonResults.AverageReview +
                    " Stars of " +
                    jsonResults.TotalReviews +
                    " ratings";
                $(ratingButton).closest(".rating").html(displayValue);

            },
            error: function() {
                alert("An error ocurred while attempting to load content.");
            },
            complete: function() {
                AtController.Loading = false;
            }
        });
    },

    // Top navigation Click Handler
    TopNavClickHandler: function(navButton) {
        var action = $(navButton).data("action");
        var title = $(navButton).text();
        if (AtController.CurrentAction !== action && !AtController.Loading) {
            AtController.Loading = true;
            AtController.CurrentAction = action;
            AtController.SetActiveNav(navButton);
            AtController.SetContentTitle(title);
            AtController.LoadPage();
            AtController.SameClickCount = 0;
        } else {
            AtController.SameClickCount++;
        }
        //TODO: if time permits play a sound instead of alert *optional humor*
        if (AtController.SameClickCount === 3) {
            alert("Stop clicking me!");
        }
    },

    // Load new page
    LoadPage: function(liquorTypeId) {
        var postVars = {};
        if (liquorTypeId !== undefined) {
            postVars.liquorTypeId = liquorTypeId;
        }
        $.ajax({
            url: "/Home/" + AtController.CurrentAction,
            data: postVars,
            type: "GET",
            success: function(partialView) {
                $(AtController.ContentBody.html(partialView));
                $(AtController.ContentBody).find("input[type=text],textarea,select").filter(":visible:first").focus();
            },
            error: function() {
                alert("An error ocurred while attempting to load content.");
            },
            complete: function() {
                AtController.Loading = false;
            }
        });
    },

    // Submit a form
    SubmitForm: function() {
        // Make sure we aren't loading another page.
        if (AtController.Loading) {
            return false;
        }
        AtController.Loading = true;
        // Get the form
        var form = $(AtController.ContentBody).find("form");
        // Serialize the variables
        var postVars = $(form).serialize();
        // Get the post target url
        var targetUrl = $(form).attr("action");
        // Last (kind of a cheat and I would do this better in prod) snag the form type to set 
        //   page title and selected button
        var formType = $("#formType").val();
        // Post the form to the server
        $.ajax({
            url: targetUrl,
            data: postVars,
            type: "Post",
            success: function(partialView) {
                // Server will either send back the new list of catgories or liquors
                // TODO: return json and trigger a click of the correct button instead of this extra code
                $(AtController.ContentBody).html(partialView);
                var navButton = $("#liquors");
                var action = "LoadLiquors";
                if (formType === "Categories") {
                    navButton = $("#liquorTypes");
                    action = "LoadCategories";
                }
                AtController.SetActiveNav(navButton);
                AtController.SetContentTitle(formType);
                AtController.CurrentAction = action;
            },
            error: function() {
                alert("An error ocurred while attempting to save.");
            },
            complete: function() {
                AtController.Loading = false;
            }
        });
    },


    // Clear current active navigation item and set new to active
    SetActiveNav: function(navButton) {
        $(AtController.TopNav).find("a").removeClass("active");
        $(navButton).addClass("active");
    },

    // Change the content title
    SetContentTitle: function(title) {
        //TODO: Transition
        $(AtController.ContentTitle).text(title);
    }

};