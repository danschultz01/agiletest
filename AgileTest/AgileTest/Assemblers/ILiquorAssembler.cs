﻿using AgileTest.Models;
using AgileTest.ViewModels;

namespace AgileTest.Assemblers
{
    interface ILiquorAssembler
    {
        LiquorVm Disassemble(Liquor liquor);
        Liquor Assemble(LiquorVm liquorVm);
        Liquor Assemble(Liquor liquor,LiquorVm liquorVm);

    }
}
