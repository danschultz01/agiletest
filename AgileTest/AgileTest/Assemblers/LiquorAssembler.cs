﻿using AgileTest.Models;
using AgileTest.ViewModels;

namespace AgileTest.Assemblers
{
    public class LiquorAssembler : ILiquorAssembler
    {
        #region Properties
        private readonly LiquorTypeAssembler _liquorTypeAssembler;
        #endregion

        #region Constructor
        public LiquorAssembler()
        {
            _liquorTypeAssembler = new LiquorTypeAssembler();
        }
        #endregion

        /// <summary>
        /// Returns a new LiquorVm from Liquor
        /// </summary>
        /// <param name="liquor"></param>
        /// <returns></returns>
        public LiquorVm Disassemble(Liquor liquor)
        {
            return new LiquorVm
            {
                Id = liquor.Id,
                Name = liquor.Name,
                Color = liquor.Color,
                Description = liquor.Description,
                LiquorTypeId = liquor.LiquorTypeId,
                TotalReviews = liquor.TotalReviews,
                TotalStars = liquor.TotalStars,
                LiquorTypeVm = _liquorTypeAssembler.Disassemble(liquor.LiquorType)
            };
        }

        /// <summary>
        /// Returns a new Liquor from LiquorVm
        /// </summary>
        /// <param name="liquorVm"></param>
        /// <returns></returns>
        public Liquor Assemble(LiquorVm liquorVm)
        {
            var liquor = new Liquor();
            return Assemble(liquor, liquorVm);
        }

        /// <summary>
        /// Returns an updated Liquor from a LiquorVm
        /// Does not update rating information which should only be altered by stored procedure
        /// </summary>
        /// <param name="liquor"></param>
        /// <param name="liquorVm"></param>
        /// <returns></returns>
        public Liquor Assemble(Liquor liquor, LiquorVm liquorVm)
        {
            liquor.Id = liquorVm.Id;
            liquor.Name = liquorVm.Name;
            liquor.Description = liquorVm.Description;
            liquor.Color = liquorVm.Color;
            liquor.LiquorTypeId = liquorVm.LiquorTypeId;
            return liquor;
        }
    }
}