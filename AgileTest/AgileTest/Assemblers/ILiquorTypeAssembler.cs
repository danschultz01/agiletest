﻿using AgileTest.Models;
using AgileTest.ViewModels;

namespace AgileTest.Assemblers
{
    interface ILiquorTypeAssembler
    {
        LiquorTypeVm Disassemble(LiquorType liquorType);
        LiquorType Assemble(LiquorTypeVm liquorTypeVm);
        LiquorType Assemble(LiquorType liquorType,LiquorTypeVm liquorTypeVm);

    }
}
