﻿using AgileTest.Models;
using AgileTest.ViewModels;

namespace AgileTest.Assemblers
{
    public class LiquorTypeAssembler : ILiquorTypeAssembler
    {
        /// <summary>
        /// Returns a new LiquorTypeVm from a LiquorType model
        /// </summary>
        /// <param name="liquorType"></param>
        /// <returns></returns>
        public LiquorTypeVm Disassemble(LiquorType liquorType)
        {
            return new LiquorTypeVm
            {
                Id = liquorType.Id,
                Name = liquorType.Name,
                Description = liquorType.Description
            };
        }

        /// <summary>
        /// Returns a new LiquorType model from a LiquorTypeVm
        /// </summary>
        /// <param name="liquorTypeVm"></param>
        /// <returns></returns>
        public LiquorType Assemble(LiquorTypeVm liquorTypeVm)
        {
            var liquorType = new LiquorType();
            return Assemble(liquorType, liquorTypeVm);
        }

        /// <summary>
        /// Updates an existing LiquorType model from a LiquorTypeVm
        /// </summary>
        /// <param name="liquorType"></param>
        /// <param name="liquorTypeVm"></param>
        /// <returns></returns>
        public LiquorType Assemble(LiquorType liquorType, LiquorTypeVm liquorTypeVm)
        {
            liquorType.Id = liquorTypeVm.Id;
            liquorType.Name = liquorTypeVm.Name;
            liquorType.Description = liquorTypeVm.Description;
            return liquorType;
        }
    }
}